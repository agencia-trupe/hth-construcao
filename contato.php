<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    exit('Forbidden.');
}

$nome     = isset($_POST['nome']) ? htmlentities($_POST['nome']) : '';
$email    = isset($_POST['email']) ? htmlentities($_POST['email']) : '';
$telefone = isset($_POST['telefone']) ? htmlentities($_POST['telefone']) : '';
$mensagem = isset($_POST['mensagem']) ? htmlentities($_POST['mensagem']) : '';

if (!$nome || !$email || !filter_var($email, FILTER_VALIDATE_EMAIL) || !$mensagem) {
    header('HTTP/1.1 422 Unprocessable Entity');
    exit('Preencha todos os campos corretamente.');
}

$mail = new PHPMailer(true);

try {
    $mail->setFrom('hometohome@grupohth.com.br', 'HTH');
    $mail->addAddress('luiz@trupe.net'); // E-MAIL DE ENVIO
    $mail->addReplyTo($email, $nome);

    $mail->isHTML(true);
    $mail->Subject = '[CONTATO] HTH';
    $mail->Body    = "<span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>".$nome."</span><br><span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>".$email."</span><br><span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>".$telefone."</span><br><span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Mensagem:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>".$mensagem."</span>";
    $mail->AltBody = $nome.' ('. $email .'): '.$mensagem;

    $mail->send();

    echo json_encode(['response' => 'Mensagem enviada com sucesso!']);
} catch (Exception $e) {
    header('HTTP/1.1 500 Internal Server Error');
    exit($mail->ErrorInfo);
}

?>
